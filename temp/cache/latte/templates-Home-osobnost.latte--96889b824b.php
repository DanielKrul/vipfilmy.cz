<?php
// source: /var/www/html/Práce/vipfilmy.cz/app/FrontModule/templates/Home/osobnost.latte

use Latte\Runtime as LR;

class Template96889b824b extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
		'title' => 'blockTitle',
	];

	public $blockTypes = [
		'content' => 'html',
		'title' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['movie'])) trigger_error('Variable $movie overwritten in foreach on line 29');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockContent($_args)
	{
		extract($_args);
		?>	<h1 class="text-center"><?php
		$this->renderBlock('title', get_defined_vars());
?>
 filmy ke stažení</h1>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3">
					<img style="width: 100%;" src="http://vipfilmy.cz/tvurci/<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl((floor($actualPersonality->id_csfd / 1000) * 1000) / 1000)) /* line 6 */ ?>/<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($actualPersonality->id_csfd)) /* line 6 */ ?>">
			</div>

			<div class="col-sm-9">
				<div class="clearfix">
					<table class="table">
						<tbody>
<?php
		if ($actualPersonality->datum_narozeni != '') {
?>
							<tr>
								<td><strong>Datum narození: </strong></td>
								<td><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->date, $actualPersonality->datum_narozeni, '%d. %m. %Y')) /* line 16 */ ?></td>
							</tr>
<?php
		}
		if ($actualPersonality->zeme != '') {
?>
							<tr>
								<td><strong>Původ: </strong></td>
								<td><?php echo LR\Filters::escapeHtmlText($actualPersonality->zeme) /* line 22 */ ?></td>
							</tr>
<?php
		}
?>
							<tr>
								<td><strong>Hrál v: </strong></td>
								<td>
									<ul class="list-inline">
<?php
		$iterations = 0;
		foreach ($moviesInfo as $movie) {
			?>											<li class="list-inline-item"><a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('detail', $movie['seo_default_name']))) /* line 30 */ ?>"><?php
			echo LR\Filters::escapeHtmlText(call_user_func($this->filters->striptags, $movie['default_name'])) /* line 30 */ ?></a></li> 
<?php
			$iterations++;
		}
?>
									</ul>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				
			</div>
		</div>

		<div class="row">
			<h2>Biografie osobnosti: </h2>
			<?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->striptags, $actualPersonality->biografie_text)) /* line 44 */ ?>

		</div>
	</div>
<?php
	}


	function blockTitle($_args)
	{
		extract($_args);
		?><span><?php echo LR\Filters::escapeHtmlText($actualPersonality->jmeno_prijmeni) /* line 2 */ ?></span><?php
	}

}
