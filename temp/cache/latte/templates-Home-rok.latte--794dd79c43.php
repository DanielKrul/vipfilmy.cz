<?php
// source: /var/www/html/Práce/vipfilmy.cz/app/FrontModule/templates/Home/rok.latte

use Latte\Runtime as LR;

class Template794dd79c43 extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
		'head' => 'blockHead',
	];

	public $blockTypes = [
		'content' => 'html',
		'head' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['movie'])) trigger_error('Variable $movie overwritten in foreach on line 5');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockContent($_args)
	{
		extract($_args);
		?>	<h1 class="text-center"><?php echo LR\Filters::escapeHtmlText($actualYear) /* line 2 */ ?> filmy ke stažení zdarma</h1>
	<div class="container-fluid">
		<div class="row">
<?php
		$iterations = 0;
		foreach ($moviesByYear as $movie) {
?>
			<div class="col-sm-6 movieItem">
				<div class="media">
					<div class="media-left media-top">
						<img alt="<?php echo LR\Filters::escapeHtmlAttr(call_user_func($this->filters->striptags, $movie['default_name'])) /* line 9 */ ?>" width="70" src="http://vipfilmy.cz/img_mov/<?php
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl((ceil($movie['id_csfd'] / 1000) * 1000) / 1000)) /* line 9 */ ?>/<?php
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($movie['img_url'])) /* line 9 */ ?>" class="media-object">
					</div>
					<div class="media-body">
						<h3 class="media-heading"><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->striptags, $movie['default_name'])) /* line 12 */ ?> <a data-toggle="tooltip" title="Detail na ČSFD" target="_blank" href="https://www.csfd.cz/film/<?php
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($movie['id_csfd'])) /* line 12 */ ?>-<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($movie['seo_default_name'])) /* line 12 */ ?>/prehled/"><small class="text-muted pull-right"><img alt="ČSFD.cz" src="<?php
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 12 */ ?>/design/icons/csfd.png" width="15"> <?php
			echo LR\Filters::escapeHtmlText($movie['hodnoceni']) /* line 12 */ ?>%</small></a></h3>
						<p><small>Rok: <?php echo LR\Filters::escapeHtmlText($movie['rok']) /* line 13 */ ?></small><br>
						<small>Žánr: <?php echo LR\Filters::escapeHtmlText($movie['zanr']) /* line 14 */ ?></small></p>
						<?php
			if (trim($movie['text']) == '') {
				?> <p>Omlouváme se, popisek není k dispozici....</p> <?php
			}
			else {
				?> <p><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->striptags, call_user_func($this->filters->truncate, $movie['text'], 330))) /* line 15 */ ?></p> <?php
			}
?>

						<div class="btn-group pull-right">
							<a data-toggle="tooltip" title="Stáhnout na hellspy" target="_blank" href="http://www.hellspy.cz/search/?usrq=<?php
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(call_user_func($this->filters->striptags, $movie['default_name']))) /* line 17 */ ?>&d=aac116" class="btn btn-success">
								<span class="glyphicon glyphicon-download-alt"></span> Stáhnout
							</a>
							<a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('detail',call_user_func($this->filters->webalize,  $movie['default_name'])))) /* line 20 */ ?>" class="btn btn-primary">
								<span class="glyphicon glyphicon-info-sign"></span> Detail
							</a>
						</div>
					</div>
				</div>
			</div>
<?php
			$iterations++;
		}
?>
		</div>
		<div class="row">
			<ul class="pagination pull-right">
<?php
		if ($actualPage > 0) {
			?>					<li><a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('rok', $actualYear, $actualPage - 1))) /* line 32 */ ?>">&larr; Předchozí</a></li>
<?php
		}
?>

<?php
		$this->renderBlock('head', get_defined_vars());
?>

<?php
		if ($actualPage > 4) {
			for ($i = 5;
			$i > 0;
			$i--) {
				?>    					<li<?php if ($_tmp = array_filter([$presenter->isLinkCurrent('rok', $actualYear, $actualPage - $i) ? 'active' : NULL])) echo ' class="', LR\Filters::escapeHtmlAttr(implode(" ", array_unique($_tmp))), '"' ?>><a href="<?php
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('rok', $actualYear, $actualPage - $i))) /* line 44 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($actualPage - $i) /* line 44 */ ?></a></li>
<?php
			}
		}
?>

<?php
		for ($i = 0;
		$i < 5;
		$i++) {
			?>    				<li<?php if ($_tmp = array_filter([$presenter->isLinkCurrent('rok', $actualYear, $actualPage + $i) ? 'active' : NULL])) echo ' class="', LR\Filters::escapeHtmlAttr(implode(" ", array_unique($_tmp))), '"' ?>><a href="<?php
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('rok', $actualYear, $actualPage + $i))) /* line 49 */ ?>"><?php
			echo LR\Filters::escapeHtmlText($actualPage + $i) /* line 49 */ ?></a></li>
<?php
		}
		?>				<li><a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('rok', $actualYear, $actualPage + 1))) /* line 51 */ ?>">Další &rarr;</a></li>
			</ul>
		</div>
	</div>
<?php
	}


	function blockHead($_args)
	{
		extract($_args);
		if ($actualPage > 0) {
			?><link rel="prev" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('rok', $actualYear, $actualPage - 1))) /* line 37 */ ?>">
<link rel="next" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('rok', $actualYear, $actualPage + 1))) /* line 38 */ ?>">
<?php
		}
		
	}

}
