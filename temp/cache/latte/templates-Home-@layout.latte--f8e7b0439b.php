<?php
// source: /var/www/html/Práce/vipfilmy.cz/app/FrontModule/templates/Home/@layout.latte

use Latte\Runtime as LR;

class Templatef8e7b0439b extends Latte\Runtime\Template
{
	public $blocks = [
		'head' => 'blockHead',
		'content' => 'blockContent',
	];

	public $blockTypes = [
		'head' => 'html',
		'content' => 'html',
	];


	function main()
	{
		extract($this->params);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php
		if (isset($this->blockQueue["title"])) {
			$this->renderBlock('title', $this->params, function ($s, $type) {
				$_fi = new LR\FilterInfo($type);
				return LR\Filters::convertTo($_fi, 'html', $this->filters->filterContent('striptags', $_fi, $s));
			});
			?> <?php
		}
		else {
			?> Filmy ke stažení zdarma | VIPFilmy.cz <?php
		}
?></title>
		<meta name="keywords" content="jaffa, kree, shakree, film, stažení">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="author" content="Daniel Krůl | VIP trust s.r.o.">
		<meta name="description" content="Na tomto webu naleznete téměř veškeré filmy, které si můžete stáhnout do svého počítače.">
		<meta name="robots" content="index,follow">
		<link rel="stylesheet" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 11 */ ?>/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 12 */ ?>/bootstrap/css/bootstrap-theme.css">
		<link rel="stylesheet" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 13 */ ?>/css/main.css">
		<script src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 14 */ ?>/js/jquery.min.js"></script>
		<script src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 15 */ ?>/bootstrap/js/bootstrap.min.js"></script>
		<!-- <script src="<?php echo LR\Filters::escapeHtmlComment($basePath) /* line 16 */ ?>/js/main.js"></script> -->
		<link rel="shortcut icon" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 17 */ ?>/favicon.ico">
		<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('head', get_defined_vars());
?>
	</head>
	<body>
		<div id="fb-root"></div>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-100099498-1', 'auto');
			ga('send', 'pageview');

			(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/cs_CZ/sdk.js#xfbml=1&version=v2.9";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
		<nav class="navbar navbar-default" id="navigace">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 48 */ ?>">
						<img alt="VIPFilmy.cz" src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 49 */ ?>/design/images/logo.png">
					</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
<?php
		$form = $_form = $this->global->formsStack[] = $this->global->uiControl["searchForm"];
		?>					<form style="margin-top: 15px;" class="navbar-form navbar-right" role="search"<?php
		echo Nette\Bridges\FormsLatte\Runtime::renderFormBegin(end($this->global->formsStack), array (
		'style' => NULL,
		'class' => NULL,
		'role' => NULL,
		), FALSE) ?>>
						<div class="form-group">
							<input type="text" id="movieSearch" class="form-control" placeholder="Vyhledej film nebo osobnost"<?php
		$_input = end($this->global->formsStack)["searchInput"];
		echo $_input->getControlPart()->addAttributes(array (
		'type' => NULL,
		'id' => NULL,
		'class' => NULL,
		'placeholder' => NULL,
		))->attributes() ?>>
						</div>
						<button class="btn btn-default" type="submit"<?php
		$_input = end($this->global->formsStack)["sendInput"];
		echo $_input->getControlPart()->addAttributes(array (
		'class' => NULL,
		'type' => NULL,
		))->attributes() ?>><i class="glyphicon glyphicon-search"></i></button>
<?php
		echo Nette\Bridges\FormsLatte\Runtime::renderFormEnd(array_pop($this->global->formsStack), FALSE);
?>					</form>
				</div>
				<!--/.nav-collapse -->
			</div>
		</nav>
		<div class="container-fluid" style="margin-left: 2em; margin-right: 2em;">
			<div class="row">
				<div class="col-sm-8">
					<div class="row">
						<div id="contentMovies">
<?php
		$this->renderBlock('content', get_defined_vars());
?>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="container-fluid">
						<div class="row" style="margin-bottom: 1.5em;">
							<div class="panel panel-changed panel-primary">
								<div class="panel-heading">
									<h2 class="text-center rightTitle">Kategorie</h2>
								</div>
								<div class="panel-body">
									<div class="list-group">
<?php
		$iterations = 0;
		foreach ($listOfCategories as $list) {
			?>											<a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('kategorie',call_user_func($this->filters->webalize,  $list['name']), 0))) /* line 88 */ ?>"<?php
			if ($_tmp = array_filter([$presenter->isLinkCurrent('kategorie', $list['name']) ? 'active' : NULL, 'list-group-item'])) echo ' class="', LR\Filters::escapeHtmlAttr(implode(" ", array_unique($_tmp))), '"' ?>>
											<span style="margin-right: 5px;" class="glyphicon glyphicon-film"></span>
											<strong><?php echo LR\Filters::escapeHtmlText($list['name']) /* line 90 */ ?></strong>
											</a>
<?php
			$iterations++;
		}
?>
									</div>
								</div>
							</div>
						</div>
						<div class="row" style="margin-bottom: 1.5em;">
							<div class="panel panel-primary panel-changed">
								<div class="panel-heading">
									<h2 class="text-center rightTitle">Statistika</h2>
								</div>
								<div class="panel-body">
									<ul class="list-group">
										<li class="list-group-item"><strong>Počet filmů</strong> <span class="badge"><?php echo LR\Filters::escapeHtmlText($numberOfMovies) /* line 107 */ ?></span></li>
										<li class="list-group-item"><strong>Osobností</strong> <span class="badge"><?php echo LR\Filters::escapeHtmlText($numberOfPersonalities) /* line 108 */ ?></span></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="bg-primary">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-6 footerleft ">
						<div class="logofooter"> <img alt="VIPFilmy.cz" src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 121 */ ?>/design/images/logo.png" style="width: 100%;"></div>
						<p>Na tomto webu naleznete téměř veškeré filmy, které si můžete stáhnout do svého počítače. Veškeré filmy jsou ve FULL HD kvalitě a některé i ve 4K rozlišení. Některé filmy a seriály zde naleznete ještě dříve než jdou do kina. Stahování je buď přímé nebo přes torrent.</p>
						<p><i class="fa fa-envelope"></i> E-mail: <a href="mailto:info@viptrust.cz">info@viptrust.cz</a></p>
					</div>
					<div class="col-md-3 col-sm-6 paddingtop-bottom pull-right">
						<div class="fb-page" data-href="https://www.facebook.com/VIPfilmycz-784154041756144/" data-tabs="timeline" data-height="300" data-small-header="false" style="margin-bottom:15px;" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
							<div class="fb-xfbml-parse-ignore">
								<blockquote cite="https://www.facebook.com/VIPfilmycz-784154041756144/"><a href="https://www.facebook.com/VIPfilmycz-784154041756144/">Facebook</a></blockquote>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<div class="copyright">
			<div class="container">
				<div class="col-md-6">
					<p>© 2017 | Web vytvořen <a href="https://viptrust.cz">VIPTrust.cz</a></p>
				</div>
			</div>
		</div>
	</body>
</html><?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['list'])) trigger_error('Variable $list overwritten in foreach on line 87');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockHead($_args)
	{
		
	}


	function blockContent($_args)
	{
		
	}

}
