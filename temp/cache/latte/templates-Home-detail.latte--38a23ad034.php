<?php
// source: /var/www/html/Práce/vipfilmy.cz/app/FrontModule/templates/Home/detail.latte

use Latte\Runtime as LR;

class Template38a23ad034 extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
		'title' => 'blockTitle',
	];

	public $blockTypes = [
		'content' => 'html',
		'title' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['zanr'])) trigger_error('Variable $zanr overwritten in foreach on line 63');
		if (isset($this->params['person'])) trigger_error('Variable $person overwritten in foreach on line 74');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockContent($_args)
	{
		extract($_args);
		?>	<h1 class="text-center"><?php
		$this->renderBlock('title', get_defined_vars());
?>
 ke stažení zdarma</h1>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3">
<?php
		if ($movieInfo['img_url'] == '') {
			?>					<img alt="<?php echo LR\Filters::escapeHtmlAttr(call_user_func($this->filters->striptags, $actualName)) /* line 7 */ ?>" style="width: 100%;" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
<?php
		}
		else {
			?>					<img alt="<?php echo LR\Filters::escapeHtmlAttr(call_user_func($this->filters->striptags, $actualName)) /* line 9 */ ?>" style="width: 100%;" src="http://vipfilmy.cz/img_mov/<?php
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl((ceil($movieInfo['id_csfd'] / 1000) * 1000) / 1000)) /* line 9 */ ?>/<?php
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($movieInfo['img_url'])) /* line 9 */ ?>">
<?php
		}
?>
			</div>

			<div class="col-sm-9">
				<div class="clearfix">
					<table class="table">
						<tbody>
<?php
		if ($movieInfo['sk_name'] != '') {
?>
							<tr>
								<td><img src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 19 */ ?>/design/icons/slovakia.png" width="25"></td>
								<td><?php echo LR\Filters::escapeHtmlText($movieInfo['sk_name']) /* line 20 */ ?></td>
							</tr>
<?php
		}
?>

<?php
		if ($movieInfo['usa_name'] != '') {
?>
							<tr>
								<td><img src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 26 */ ?>/design/icons/usa.png" width="25"></td>
								<td><?php echo LR\Filters::escapeHtmlText($movieInfo['usa_name']) /* line 27 */ ?></td>
							</tr>
<?php
		}
?>

<?php
		if ($movieInfo['british_name'] != '') {
?>
							<tr>
								<td><img src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 33 */ ?>/design/icons/britain.png" width="25"></td>
								<td><?php echo LR\Filters::escapeHtmlText($movieInfo['british_name']) /* line 34 */ ?></td>
							</tr>
<?php
		}
?>

<?php
		if ($movieInfo['kanada_name'] != '') {
?>
							<tr>
								<td><img src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 40 */ ?>/design/icons/canada.png" width="25"></td>
								<td><?php echo LR\Filters::escapeHtmlText($movieInfo['kanada_name']) /* line 41 */ ?></td>
							</tr>
<?php
		}
?>

<?php
		if ($movieInfo['francie_name'] != '') {
?>
							<tr>
								<td><img src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 47 */ ?>/design/icons/france.png" width="25"></td>
								<td><?php echo LR\Filters::escapeHtmlText($movieInfo['francie_name']) /* line 48 */ ?></td>
							</tr>						
<?php
		}
?>

<?php
		if ($movieInfo['rusko_name'] != '') {
?>
							<tr>
								<td><img src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 54 */ ?>/design/icons/russia.png" width="25"></td>
								<td><?php echo LR\Filters::escapeHtmlText($movieInfo['rusko_name']) /* line 55 */ ?></td>
							</tr>						
<?php
		}
?>

							<tr>
								<td><strong>Žánr: </strong></td>
								<td>
									<ul class="list-inline">
<?php
		$iterations = 0;
		foreach ($zanrInfo as $zanr) {
			?>										<li><a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('kategorie',call_user_func($this->filters->webalize,  trim($zanr)), 0))) /* line 64 */ ?>"><?php
			echo LR\Filters::escapeHtmlText($zanr) /* line 64 */ ?></a></li> 
<?php
			$iterations++;
		}
?>
									</ul>
								</td>
							</tr>

							<tr>
								<td><strong>Osobnosti: </strong></td>
								<td>
									<ul class="list-inline">
<?php
		$iterations = 0;
		foreach ($personalitiesInfo as $person) {
			?>										<li class="list-inline-item"><a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('osobnost', $person['seoName']))) /* line 75 */ ?>"><?php
			echo LR\Filters::escapeHtmlText($person['jmeno_prijmeni']) /* line 75 */ ?></a></li> 
<?php
			$iterations++;
		}
?>
									</ul>
								</td>
							</tr>

							<tr>
								<td><strong>Hodnocení: </strong></td>
								<td>
									<a data-toggle="tooltip" title="Filmy podle hodnocení" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('hodnoceni', $movieInfo['hodnoceni']))) /* line 84 */ ?>"><?php
		echo LR\Filters::escapeHtmlText($movieInfo['hodnoceni']) /* line 84 */ ?>%</a>
								</td>
							</tr>

							<tr>
								<td><strong>Rok: </strong></td>
								<td>
									<a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('rok', $movieInfo['rok'], 0))) /* line 91 */ ?>"><?php
		echo LR\Filters::escapeHtmlText($movieInfo['rok']) /* line 91 */ ?></a>
								</td>
							</tr>

						</tbody>
					</table>
				</div>
				
			</div>
		</div>

		<div class="row">
			<h2 class="text-center">Popisek filmu:</h2>
			<?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->striptags, $movieInfo['text'])) /* line 104 */ ?>

		</div>
		<div class="row text-center" style="margin-top: 1em;">
			<div class="btn-group">
				<a data-toggle="tooltip" title="Stáhnout na hellspy" target="_blank" href="http://www.hellspy.cz/search/?usrq=<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(call_user_func($this->filters->striptags, $movieInfo['default_name']))) /* line 108 */ ?>&d=aac116" class="btn btn-lg btn-success">
					<span class="glyphicon glyphicon-download-alt"></span> Stáhnout
				</a>
			</div>
		</div>
	</div>
<?php
	}


	function blockTitle($_args)
	{
		extract($_args);
		?><span><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->striptags, $actualName)) /* line 2 */ ?></span><?php
	}

}
