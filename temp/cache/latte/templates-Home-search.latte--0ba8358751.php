<?php
// source: /var/www/html/Práce/vipfilmy.cz/app/FrontModule/templates/Home/search.latte

use Latte\Runtime as LR;

class Template0ba8358751 extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
		'title' => 'blockTitle',
		'head' => 'blockHead',
	];

	public $blockTypes = [
		'content' => 'html',
		'title' => 'html',
		'head' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['item'])) trigger_error('Variable $item overwritten in foreach on line 24');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockContent($_args)
	{
		extract($_args);
		?><h1 class="text-center"><?php
		$this->renderBlock('title', get_defined_vars());
		?><br><small>Počet výsledků: <?php echo LR\Filters::escapeHtmlText($gettedDataCount) /* line 2 */ ?></small></h1>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="btn-group pull-right">
				<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
					Vyhledávat ve: <span class="caret"></span></button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('search', 'film',call_user_func($this->filters->striptags,  $actualSearch), $actualPage))) /* line 11 */ ?>">Filmy</a></li>
						<li><a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('search', 'osobnost',call_user_func($this->filters->striptags,  $actualSearch), $actualPage))) /* line 12 */ ?>">Osobnosti</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
	</div>

	<div class="row">
<?php
		$iterations = 0;
		foreach ($gettedData as $item) {
			if ($actualType == 'film') {
?>
				<div class="col-sm-6 movieItem">
				<div class="media">
					<div class="media-left media-top">
<?php
				if ($item['img_url'] == '') {
					?>							<img alt="<?php echo LR\Filters::escapeHtmlAttr(call_user_func($this->filters->striptags, $item['default_name'])) /* line 30 */ ?>" width="70" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" class="media-object">
<?php
				}
				else {
					?>							<img alt="<?php echo LR\Filters::escapeHtmlAttr(call_user_func($this->filters->striptags, $item['default_name'])) /* line 32 */ ?>" width="70" src="http://vipfilmy.cz/img_mov/<?php
					echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl((ceil($item['id_csfd'] / 1000) * 1000) / 1000)) /* line 32 */ ?>/<?php
					echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($item['img_url'])) /* line 32 */ ?>" class="media-object">
<?php
				}
?>
					</div>
					<div class="media-body">
						<h3 class="media-heading"><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->striptags, $item['default_name'])) /* line 36 */ ?> <a data-toggle="tooltip" title="Detail na ČSFD" target="_blank" href="https://www.csfd.cz/film/<?php
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($item['id_csfd'])) /* line 36 */ ?>-<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($item['seo_default_name'])) /* line 36 */ ?>/prehled/"><small class="text-muted pull-right"><img alt="ČSFD.cz" src="<?php
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 36 */ ?>/design/icons/csfd.png" width="15"> <?php
				echo LR\Filters::escapeHtmlText($item['hodnoceni']) /* line 36 */ ?>%</small></a></h3>
						<p><small>Rok: <?php echo LR\Filters::escapeHtmlText($item['rok']) /* line 37 */ ?></small><br>
						<small>Žánr: <?php echo LR\Filters::escapeHtmlText($item['zanr']) /* line 38 */ ?></small></p>
						<?php
				if (trim($item['text']) == '') {
					?> <p>Omlouváme se, popisek není k dispozici....</p> <?php
				}
				else {
					?> <p><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->striptags, call_user_func($this->filters->truncate, $item['text'], 330))) /* line 39 */ ?></p> <?php
				}
?>

						<div class="btn-group pull-right">
							<a data-toggle="tooltip" title="Stáhnout na hellspy" target="_blank" href="http://www.hellspy.cz/search/?usrq=<?php
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(call_user_func($this->filters->striptags, $item['default_name']))) /* line 41 */ ?>&d=aac116" class="btn btn-success">
								<span class="glyphicon glyphicon-download-alt"></span> Stáhnout
							</a>
							<a target="_blank" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('detail',call_user_func($this->filters->webalize,  $item['default_name'])))) /* line 44 */ ?>" class="btn btn-primary">
								<span class="glyphicon glyphicon-info-sign"></span> Detail
							</a>
						</div>
					</div>
				</div>
			</div>
<?php
			}
			elseif ($actualType == 'osobnost') {
?>
				<div class="col-sm-6 movieItem">
				<div class="media">
					<div class="media-left media-top">
						<img alt="<?php echo LR\Filters::escapeHtmlAttr(call_user_func($this->filters->striptags, $item['jmeno_prijmeni'])) /* line 55 */ ?>" width="70" src="http://vipfilmy.cz/tvurci/<?php
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl((floor($item['id_csfd'] / 1000) * 1000) / 1000)) /* line 55 */ ?>/<?php
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($item['id_csfd'])) /* line 55 */ ?>" class="media-object">
					</div>
					<div class="media-body">
						<h3 class="media-heading"><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->striptags, $item['jmeno_prijmeni'])) /* line 58 */ ?></h3>
						<p><small>Datum narození: <?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->date, $item['datum_narozeni'], '%d. %m. %Y')) /* line 59 */ ?></small><br>
						<small>Původ: <?php echo LR\Filters::escapeHtmlText($item['zeme']) /* line 60 */ ?></small></p>
						<p><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->striptags, call_user_func($this->filters->truncate, $item['biografie_text'], 330))) /* line 61 */ ?></p>
						<div class="btn-group pull-right">
							<a target="_blank" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('osobnost', $item['seoName']))) /* line 63 */ ?>" class="btn btn-primary">
								<span class="glyphicon glyphicon-info-sign"></span> Detail
							</a>
						</div>
					</div>
				</div>
				</div>
<?php
			}
			$iterations++;
		}
?>
	</div>
	<div class="row">
			<ul class="pagination pull-right">
<?php
		if ($actualPage > 0) {
			?>					<li><a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('search', $actualType,call_user_func($this->filters->striptags,  $actualSearch), $actualPage - 1))) /* line 76 */ ?>">&larr; Předchozí</a></li>
<?php
		}
?>

<?php
		$this->renderBlock('head', get_defined_vars());
?>

<?php
		if ($actualPage > 4) {
			for ($i = 5;
			$i > 0;
			$i--) {
				?>    					<li<?php if ($_tmp = array_filter([$presenter->isLinkCurrent('search', $actualType,call_user_func($this->filters->striptags,  $actualSearch), $actualPage - $i) ? 'active' : NULL])) echo ' class="', LR\Filters::escapeHtmlAttr(implode(" ", array_unique($_tmp))), '"' ?>><a href="<?php
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('search', $actualType,call_user_func($this->filters->striptags,  $actualSearch), $actualPage - $i))) /* line 91 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($actualPage - $i) /* line 91 */ ?></a></li>
<?php
			}
		}
?>

<?php
		for ($i = 0;
		$i < 5;
		$i++) {
			if (($gettedDataCount / $defaultNumberOfMoviesDisplay) < ($actualPage + $i)) break;
			?>    				<li<?php if ($_tmp = array_filter([$presenter->isLinkCurrent('search', $actualType,call_user_func($this->filters->striptags,  $actualSearch), $actualPage + $i) ? 'active' : NULL])) echo ' class="', LR\Filters::escapeHtmlAttr(implode(" ", array_unique($_tmp))), '"' ?>><a href="<?php
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('search', $actualType,call_user_func($this->filters->striptags,  $actualSearch), $actualPage + $i))) /* line 97 */ ?>"><?php
			echo LR\Filters::escapeHtmlText($actualPage + $i) /* line 97 */ ?></a></li>
<?php
		}
		if (($gettedDataCount / $defaultNumberOfMoviesDisplay) > ($actualPage + 1)) {
			?>					<li><a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('search', $actualType,call_user_func($this->filters->striptags,  $actualSearch), $actualPage + 1))) /* line 100 */ ?>">Další &rarr;</a></li>
<?php
		}
?>
			</ul>
		</div>
</div>
<?php
	}


	function blockTitle($_args)
	{
		extract($_args);
		?><span>Vyhledání výrazu: <?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->striptags, $actualSearch)) /* line 2 */ ?></span><?php
	}


	function blockHead($_args)
	{
		extract($_args);
		if ($actualPage > 0) {
			?><link rel="prev" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('search', $actualType,call_user_func($this->filters->striptags,  $actualSearch), $actualPage - 1))) /* line 81 */ ?>">
<?php
		}
?>

<?php
		if (($gettedDataCount / $defaultNumberOfMoviesDisplay) > ($actualPage + 1)) {
			?><link rel="next" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('search', $actualType,call_user_func($this->filters->striptags,  $actualSearch), $actualPage + 1))) /* line 85 */ ?>">
<?php
		}
		
	}

}
