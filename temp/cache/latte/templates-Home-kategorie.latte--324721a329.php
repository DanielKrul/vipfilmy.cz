<?php
// source: /var/www/html/Práce/vipfilmy.cz/app/FrontModule/templates/Home/kategorie.latte

use Latte\Runtime as LR;

class Template324721a329 extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
		'title' => 'blockTitle',
		'head' => 'blockHead',
	];

	public $blockTypes = [
		'content' => 'html',
		'title' => 'html',
		'head' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['movie'])) trigger_error('Variable $movie overwritten in foreach on line 21');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockContent($_args)
	{
		extract($_args);
		?>	<h1 class="text-center">Kategorie: <?php
		$this->renderBlock('title', get_defined_vars());
?>
</h1>
	<div class="container-fluid">
		<div class="row">
			<div class="btn-group pull-right">
				<div class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
					Seřadit podle <span class="caret"></span></button>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('kategorie', $actualGenreWebalize, $actualPage, 'hodnoceni DESC'))) /* line 10 */ ?>">Nejlepší</a></li>
							<li><a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('kategorie', $actualGenreWebalize, $actualPage, 'rok DESC'))) /* line 11 */ ?>">Nejnovější</a></li>
							<li><a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('kategorie', $actualGenreWebalize, $actualPage, 'rok ASC'))) /* line 12 */ ?>">Nejstarší</a></li>
							<li><a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('kategorie', $actualGenreWebalize, $actualPage, 'hodnoceni ASC'))) /* line 13 */ ?>">Nejhorší</a></li>
							<li><a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('kategorie', $actualGenreWebalize, $actualPage, 'default_name DESC'))) /* line 14 */ ?>">Název</a></li>
						</ul>
					</div>
				</div>
			</div>

		<div class="row">
<?php
		$iterations = 0;
		foreach ($listOfMovies as $movie) {
?>
			<div class="col-sm-6 movieItem">
				<div class="media">
					<div class="media-left media-top">
<?php
			if ($movie['img_url'] == '') {
				?>							<img alt="<?php echo LR\Filters::escapeHtmlAttr(call_user_func($this->filters->striptags, $movie['default_name'])) /* line 26 */ ?>" width="70" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" class="media-object">
<?php
			}
			else {
				?>							<img alt="<?php echo LR\Filters::escapeHtmlAttr(call_user_func($this->filters->striptags, $movie['default_name'])) /* line 28 */ ?>" width="70" src="http://vipfilmy.cz/img_mov/<?php
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl((ceil($movie['id_csfd'] / 1000) * 1000) / 1000)) /* line 28 */ ?>/<?php
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($movie['img_url'])) /* line 28 */ ?>" class="media-object">
<?php
			}
?>
					</div>
					<div class="media-body">
						<h3 class="media-heading"><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->striptags, $movie['default_name'])) /* line 32 */ ?> <a data-toggle="tooltip" title="Detail na ČSFD" target="_blank" href="https://www.csfd.cz/film/<?php
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($movie['id_csfd'])) /* line 32 */ ?>-<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($movie['seo_default_name'])) /* line 32 */ ?>/prehled/"><small class="text-muted pull-right"><img alt="ČSFD.cz" src="<?php
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 32 */ ?>/design/icons/csfd.png" width="15"> <?php
			echo LR\Filters::escapeHtmlText($movie['hodnoceni']) /* line 32 */ ?>%</small></a></h3>
						<p><small>Rok: <?php echo LR\Filters::escapeHtmlText($movie['rok']) /* line 33 */ ?></small><br>
						<small>Žánr: <?php echo LR\Filters::escapeHtmlText($movie['zanr']) /* line 34 */ ?></small></p>
						<?php
			if (trim($movie['text']) == '') {
				?> <p>Omlouváme se, popisek není k dispozici....</p> <?php
			}
			else {
				?> <p><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->striptags, call_user_func($this->filters->truncate, $movie['text'], 330))) /* line 35 */ ?></p> <?php
			}
?>

						<div class="btn-group pull-right">
							<a data-toggle="tooltip" title="Stáhnout na hellspy" target="_blank" href="http://www.hellspy.cz/search/?usrq=<?php
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(call_user_func($this->filters->striptags, $movie['default_name']))) /* line 37 */ ?>&d=aac116" class="btn btn-success">
								<span class="glyphicon glyphicon-download-alt"></span> Stáhnout
							</a>
							<a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('detail',call_user_func($this->filters->webalize,  $movie['default_name'])))) /* line 40 */ ?>" class="btn btn-primary">
								<span class="glyphicon glyphicon-info-sign"></span> Detail
							</a>
						</div>
					</div>
				</div>
			</div>
<?php
			$iterations++;
		}
?>
		</div>
		<div class="row">
			<ul class="pagination pull-right">
<?php
		if ($actualPage > 0) {
			?>					<li><a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('kategorie',call_user_func($this->filters->webalize,  $actualGenre), $actualPage - 1), $actualSort)) /* line 52 */ ?>">&larr; Předchozí</a></li>
<?php
		}
?>

<?php
		$this->renderBlock('head', get_defined_vars());
?>

<?php
		if ($actualPage > 4) {
			for ($i = 5;
			$i > 0;
			$i--) {
				?>    					<li<?php if ($_tmp = array_filter([$presenter->isLinkCurrent('kategorie',call_user_func($this->filters->webalize,  $actualGenre), $actualPage - $i, $actualSort) ? 'active' : NULL])) echo ' class="', LR\Filters::escapeHtmlAttr(implode(" ", array_unique($_tmp))), '"' ?>><a href="<?php
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('kategorie',call_user_func($this->filters->webalize,  $actualGenre), $actualPage - $i, $actualSort))) /* line 67 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($actualPage - $i) /* line 67 */ ?></a></li>
<?php
			}
		}
?>

<?php
		for ($i = 0;
		$i < 5;
		$i++) {
			if (($gettedDataCount / $defaultNumberOfMoviesDisplay) < ($actualPage + $i)) break;
			?>    				<li<?php if ($_tmp = array_filter([$presenter->isLinkCurrent('kategorie',call_user_func($this->filters->webalize,  $actualGenre), $actualPage + $i, $actualSort) ? 'active' : NULL])) echo ' class="', LR\Filters::escapeHtmlAttr(implode(" ", array_unique($_tmp))), '"' ?>><a href="<?php
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('kategorie',call_user_func($this->filters->webalize,  $actualGenre), $actualPage + $i, $actualSort))) /* line 73 */ ?>"><?php
			echo LR\Filters::escapeHtmlText($actualPage + $i) /* line 73 */ ?></a></li>
<?php
		}
		if (($gettedDataCount / $defaultNumberOfMoviesDisplay) > ($actualPage + 1)) {
			?>					<li><a href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('kategorie',call_user_func($this->filters->webalize,  $actualGenre), $actualPage + 1), $actualSort)) /* line 76 */ ?>">Další &rarr;</a></li>
<?php
		}
?>
			</ul>
		</div>


	</div>
<?php
	}


	function blockTitle($_args)
	{
		extract($_args);
		?><span><?php echo LR\Filters::escapeHtmlText($actualGenre) /* line 2 */ ?></span><?php
	}


	function blockHead($_args)
	{
		extract($_args);
		if ($actualPage > 0) {
			?><link rel="prev" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('kategorie',call_user_func($this->filters->webalize,  $actualGenre), $actualPage - 1), $actualSort)) /* line 57 */ ?>">
<?php
		}
?>

<?php
		if (($gettedDataCount / $defaultNumberOfMoviesDisplay) > ($actualPage + 1)) {
			?><link rel="next" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($presenter->link('kategorie',call_user_func($this->filters->webalize,  $actualGenre), $actualPage + 1), $actualSort)) /* line 61 */ ?>">
<?php
		}
		
	}

}
