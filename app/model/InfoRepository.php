<?php
/**
 * Author: Daniel Krůl
 * Website: http://danielkrul.com
 */
use \Nette\Utils\Strings;
class InfoRepository extends Repository {
	public function getNumberOfMoviesAndPersonalities($type, $genre = null) {
		switch ($type) {
			case 'movies':
				$result = $this->connection->table('filmy');
				break;

			case 'personalities':
				$result = $this->connection->table('filmy_tvurci');
				break;

			case 'genre':
				$result = $this->connection->table('filmy')->where('zanr LIKE ?', '%'. $genre .'%');
		}

		return $result->count('*');
	}

	public function getListOfCategories(){
		return $this->connection->table('kategorie');
	}

	public function getListOfMoviesByOrder($order, $offset, $page){
		return $this->connection->table('filmy')->where('rok != ? AND img_url != ?', 'null', '')->order($order)->limit($offset, $page * $offset);
	}

	public function getMoviesBySpecific($id, $param, $offset, $page){
		return $this->connection->table('filmy')->where($id . ' = ? AND kategorie != ?', $param, 'epizoda')->limit($offset, $page * $offset);
	}

	public function getRealGenre($name){
		return $this->connection->table('kategorie')->where('seo_name = ?', $name)->fetch();
	}

	public function movieInfo($name){
		return $this->connection->table('filmy')->where('seo_default_name = ?', $name)->fetch();
	}

	public function listOfMoviesInCurrentCategory($id, $page, $offset, $sortBy, $count = false){
		if($count){
			return $this->connection->table('filmy')->where('zanr LIKE ? AND kategorie != ?', '%'. $id .'%', 'epizoda')->order($sortBy)->count('*');
		} else {
			return $this->connection->table('filmy')->where('zanr LIKE ? AND kategorie != ?', '%'. $id .'%', 'epizoda')->limit($offset, $page * $offset)->order($sortBy);
		}

	}

	public function getPersonalityInfo($id){
		return $this->connection->table('filmy_tvurci')->where('id_csfd', $id);
	}

	public function getActualPersonalityInfo($name){
		return $this->connection->table('filmy_tvurci')->where('seoName = ?', $name)->fetch();
	}

	public function getSpecificMovieInfo($id){
		return $this->connection->table('filmy')->where('id_csfd', $id);
	}

	public function searchByColumn($columns, $table, $page, $offset, $sortBy = 'id', $count = false){
		if($count){
			$result = $this->connection->table($table)->whereOr($columns)->limit($offset, $page * $offset)->order($sortBy);

			return [$result, $result->count('*')];
		} else {
			return [$this->connection->table($table)->whereOr($columns)->limit($offset, $page * $offset)->order($sortBy), null];
		}
	}

	public function skript(){
		$books = $this->connection->table('kategorie');
		foreach ($books as $book) {
			$this->connection->table('kategorie')->where('id = ?', $book->id)->update(array(
				'seo_name' => Strings::webalize($book->name)
				)
			);
		}
		dump('Hotovo');
	}
}