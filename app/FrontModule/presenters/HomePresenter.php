<?php

namespace FrontModule;
use \Nette\Application\UI;
use \Nette\Utils\Strings;
use Instante;

class HomePresenter extends BasePresenter {
	/** @var \InfoRepository */
	private $infoRepository;

	private $numberOfMoviesInCategories;
	private $listOfCategories;
	private $defaultNumberOfMoviesDisplay;

	public function __construct(\InfoRepository $infoRepository) {
		$this->infoRepository = $infoRepository;
		$this->listOfCategories = $this->infoRepository->getListOfCategories();
		$this->numberOfMoviesInCategories = array();
		$this->defaultNumberOfMoviesDisplay = 25;

		foreach ($this->listOfCategories as $category) {
			//array_push($this->numberOfMoviesInCategories, number_format($this->infoRepository->getNumberOfMoviesAndPersonalities('genre', $category['name']), 0, '', ' '));
		}
	}

	protected function createComponentSearchForm() { //  továrna formuláře

        $form = new UI\Form;
        $form->addText('searchInput')->setRequired('Prázdné pole!');
    	$form->addSubmit('sendInput');
    	$form->onSuccess[] = [$this, 'searchFormSubmitted'];
    	return $form;
    }

    public function searchFormSubmitted($form){
		$values = $form->getValues();
		$this->redirect('search', array('type' => 'film', 'page' => 0,'text' => $values['searchInput']));
	}

	public function beforeRender() {
		parent::beforeRender();
		$this->template->numberOfMovies = number_format($this->infoRepository->getNumberOfMoviesAndPersonalities('movies'), 0, '', ' ');
		$this->template->numberOfPersonalities = number_format($this->infoRepository->getNumberOfMoviesAndPersonalities('personalities'), 0, '', ' ');
		$this->template->listOfCategories = $this->listOfCategories;
		$this->template->numberOfMoviesInCategory = $this->numberOfMoviesInCategories;
	}

	public function actionSearch($type, $text, $page){
		$this->template->actualSearch = $text;
		$this->template->actualPage = $page;
		$this->template->actualType = $type;
		$this->template->defaultNumberOfMoviesDisplay = $this->defaultNumberOfMoviesDisplay;

		switch ($type) {
			case 'film':
				$gettedData = $this->infoRepository->searchByColumn(['default_name LIKE' => '%'.$text.'%', 'sk_name LIKE' => '%'.$text.'%', 'usa_name LIKE' => '%'.$text.'%', 'british_name LIKE' => '%'.$text.'%', 'kanada_name LIKE' => '%'.$text.'%', 'francie_name LIKE' => '%'.$text.'%', 'rusko_name LIKE' => '%'.$text.'%'], 'filmy', $page, $this->defaultNumberOfMoviesDisplay, 'hodnoceni DESC', true);
				break;

			case 'osobnost':
				$gettedData = $this->infoRepository->searchByColumn(['jmeno_prijmeni LIKE' => '%'.$text.'%'], 'filmy_tvurci', $page, $this->defaultNumberOfMoviesDisplay, 'id', true);
				break;
		}

		$this->template->gettedData = $gettedData[0];
		$this->template->gettedDataCount = $gettedData[1];
	}

	public function actionFilm($page){
		$this->template->newestMovies = $this->infoRepository->getListOfMoviesByOrder('rok DESC', $this->defaultNumberOfMoviesDisplay, $page);
		$this->template->actualPage = $page;
	}

	public function actionKategorie($name, $page, $sort){
		$actualGenre = $this->infoRepository->getRealGenre($name)->name;
		$actualGenreWebalize = $this->infoRepository->getRealGenre($name)->seo_name;

		$this->template->actualGenre = $actualGenre;
		$this->template->listOfMovies = $this->infoRepository->listOfMoviesInCurrentCategory($actualGenre, $page, $this->defaultNumberOfMoviesDisplay, $sort, false);
		$this->template->gettedDataCount = $this->infoRepository->listOfMoviesInCurrentCategory($actualGenre, $page, $this->defaultNumberOfMoviesDisplay, $sort, true);
		$this->template->defaultNumberOfMoviesDisplay = $this->defaultNumberOfMoviesDisplay;
		$this->template->actualPage = $page;
		$this->template->actualGenreWebalize = $actualGenreWebalize;
		$this->template->actualSort = $sort;
		$this->template->defaultNumberOfMoviesDisplay = $this->defaultNumberOfMoviesDisplay;
	}

	public function actionDetail($name){
		$movieInfoCache = $this->infoRepository->movieInfo($name);
		$this->template->actualName = $movieInfoCache->default_name;
		$this->template->movieId = $name;
		$this->template->movieInfo = $movieInfoCache;
		$this->template->zanrInfo = explode('/', $movieInfoCache->zanr);

		$personalitiesInfo = explode(',', $movieInfoCache->spolupracovali);
		$personalitiesInfoArray = array();

		foreach ($personalitiesInfo as $personality) {
			array_push($personalitiesInfoArray, $personality);
		}

		$this->template->personalitiesInfo = $this->infoRepository->getPersonalityInfo($personalitiesInfoArray);
	}

	public function actionOsobnost($name){
		$actualPersonalityCache = $this->infoRepository->getActualPersonalityInfo($name);
		$this->template->actualPersonality = $actualPersonalityCache;
		$moviesInfo = explode(',', $actualPersonalityCache->pusobil);
		$moviesInfoArray = array();

		foreach ($moviesInfo as $movie) {
			array_push($moviesInfoArray, $movie);
		}

		$this->template->moviesInfo = $this->infoRepository->getSpecificMovieInfo($moviesInfoArray);
	}

	public function actionRok($year, $page){
		$this->template->actualYear = $year;
		$this->template->moviesByYear = $this->infoRepository->getMoviesBySpecific('rok', $year, $this->defaultNumberOfMoviesDisplay, $page);
		$this->template->actualPage = $page;
	}

	public function actionHodnoceni($percent, $page){
		$this->template->actualPercent = $percent;
		$this->template->moviesByPercent = $this->infoRepository->getMoviesBySpecific('hodnoceni', $percent, $this->defaultNumberOfMoviesDisplay, $page);
		$this->template->actualPage = $page;
	}

	public function actionSkript(){
		$this->infoRepository->skript();
	}

}