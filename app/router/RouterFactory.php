<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;

		// Front

		$router[] = new Route('search/<type>/<text>/<page>/', array(
			'module' => 'Front',
			'presenter' => 'Home',
			'action' => 'search',
			'type' => 'film',
			'page' => 0
			));

		$router[] = new Route('osobnost[/<name>]/', array(
			'module' => 'Front',
			'presenter' => 'Home',
			'action' => 'osobnost'
			));

		$router[] = new Route('detail[/<name>]/', array(
			'module' => 'Front',
			'presenter' => 'Home',
			'action' => 'detail'
			));

		$router[] = new Route('rok[/<year>][/<page=0>]/', array(
			'module' => 'Front',
			'presenter' => 'Home',
			'action' => 'rok'
			));

		$router[] = new Route('hodnoceni[/<percent>][/<page=0>]/', array(
			'module' => 'Front',
			'presenter' => 'Home',
			'action' => 'hodnoceni'
			));

		$router[] = new Route('kategorie/<name>/<page>/<sort>/', array(
			'module' => 'Front',
			'presenter' => 'Home',
			'action' => 'kategorie',
			'page' => 0,
			'sort' => 'hodnoceni DESC'
			));

		$router[] = new Route('film/<page>/', array(
			'module' => 'Front',
			'presenter' => 'Home',
			'action' => 'film',
			'page' => 0
			));
		$router[] = new Route('<action>', array(
			'module' => 'Front',
			'presenter' => 'Home',
			'action' => 'film'
			));
		return $router;
	}

}
